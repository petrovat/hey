import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the AboutUsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html'
})
export class AboutUsPage {

  contactUsRoot = 'ContactUsPage'
  servicesRoot = 'ServicesPage'
  askRoot = 'AskPage'


  constructor(public navCtrl: NavController) {}

}
